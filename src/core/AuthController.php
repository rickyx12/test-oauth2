<?php

namespace test\oauth2\core;

use test\oauth2\service\auth\AuthServiceInterface;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\Request;
use xvsys\autoresponse\RedirectException;

/**
 * Class AuthController
 */
class AuthController{

    /**
     * @var AuthServiceInterface
     */
    private $authService;


    /**
     * @var $request Request
     */
    private $request;


    /**
     * @param \test\oauth2\service\auth\AuthServiceInterface $authService
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @inject("authService","\test\oauth2\service\auth\AuthService")
     */
    public function setAuthService(AuthServiceInterface $authService, Request $request){
        $this->authService = $authService;
        $this->request = $request;
        /**
         * @var $accessToken AccessToken
         */
        $accessToken = $this->request->getSession()->get('access_token');
        if ($accessToken === null || $accessToken->hasExpired()) {
            $authorizationUrl = $this->authService->getAuthorizationUrl();
            $this->request->getSession()->set('state', $this->authService->getState());
            throw  new RedirectException($authorizationUrl);
        }
    }


}
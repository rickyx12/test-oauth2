<?php

namespace test\oauth2\auth;

use test\oauth2\exception\UnauthorizedAccessException;
use test\oauth2\service\auth\AuthServiceInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


class Authorization {


    /**
     * @var \test\oauth2\service\auth\AuthServiceInterface
     */
    private $authService;


    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    private $urlRedirect;

    private $accessToken;

    /**
     * SessionManager constructor.
     * @param AuthServiceInterface $authService
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @Inject("authService","\test\oauth2\service\auth\AuthService")
     */
    public function __construct(AuthServiceInterface $authService, Request $request){
        $this->authService = $authService;
        $this->request = $request;
    }

    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken;
    }

    public function setUrlRedirect($urlRedirect) {
        $this->urlRedirect = $urlRedirect;
    }

    /**
     * @return string|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function authorize($url) {
        try {
            $queryParams = $this->request->query;
            $paramState = $queryParams->get('state');
            $sessionState = $this->request->getSession()->get('state');
            $accessToken = $this->authService->getToken($queryParams->get('code'), $paramState, $sessionState);
            $this->request->getSession()->set('access_token',$accessToken);
            return new RedirectResponse($url);
        } catch (UnauthorizedAccessException $e) {
            return "Sie haben keine Zugriffsrechte.";
        }
    }


    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(){
        $this->request->getSession()->clear();
        return new RedirectResponse('/clients/');
    }
}
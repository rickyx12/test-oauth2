<?php

namespace test\oauth2\service\auth;

use test\oauth2\exception\InvalidStateException;
use League\OAuth2\Client\Provider\GenericProvider;


class AuthService implements AuthServiceInterface{


    /**
     * @var \League\OAuth2\Client\Provider\GenericProvider
     */
    private $provider;


    /**
     * AuthService constructor.
     * @param \League\OAuth2\Client\Provider\GenericProvider $provider
     */
    public function __construct(
        GenericProvider $provider
    ){

        $this->provider = $provider;
    }


    /**
     * @param $code
     * @param $paramState
     * @param $sessionState
     * @return \League\OAuth2\Client\Token\AccessToken
     * @throws \test\oauth2\exception\InvalidStateException
     */
    public function getToken($code, $paramState, $sessionState){

        if ((!$paramState || !$sessionState) && $paramState !== $sessionState) {
            throw new InvalidStateException('Invalid State');
        }

        // Try to get an access token using the authorization code grant.
        return $this->provider->getAccessToken('authorization_code', [
            'code' => $code
        ]);

    }


    /**
     * @return string
     */
    public function getState(){
        return $this->provider->getState();
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl(){
        return $this->provider->getAuthorizationUrl();
    }


}
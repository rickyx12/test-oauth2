<?php

namespace test\oauth2\service\auth;


interface AuthServiceInterface{


    /**
     * @param $code
     * @param $paramState
     * @param $sessionState
     * @return mixed
     */
    public function getToken($code, $paramState, $sessionState);

    /**
     * @return mixed
     */
    public function getState();

    /**
     * @return mixed
     */
    public function getAuthorizationUrl();


}